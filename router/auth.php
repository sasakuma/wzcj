<?php

if( !defined( 'ABSPATH' ) ) exit;

class WP_REST_Auth_Router extends WP_REST_Controller {

	public function __construct( ) {
		$this->namespace     = 'mp/v1';
	}

	public function register_routes() {

		register_rest_route( $this->namespace, '/tencent/login', array(
			array(
				'methods'             	=> WP_REST_Server::CREATABLE,
				'callback'            	=> array( $this, 'wp_qq_user_auth_login' ),
				'permission_callback' 	=> array( $this, 'wp_login_permissions_check' ),
				'args'                	=> $this->wp_user_login_collection_params(),
			)
		));

		register_rest_route( $this->namespace, '/baidu/login', array(
			array(
				'methods'             	=> WP_REST_Server::CREATABLE,
				'callback'            	=> array( $this, 'wp_baidu_user_auth_login' ),
				'permission_callback' 	=> array( $this, 'wp_login_permissions_check' ),
				'args'                	=> $this->wp_user_login_collection_params(),
			)
		));

		register_rest_route( $this->namespace, '/toutiao/login', array(
			array(
				'methods'             	=> WP_REST_Server::CREATABLE,
				'callback'            	=> array( $this, 'wp_toutiao_user_auth_login' ),
				'permission_callback' 	=> array( $this, 'wp_login_permissions_check' ),
				'args'                	=> $this->wp_user_login_collection_params(),
			)
		));

	}

	public function wp_login_permissions_check( $request ) {

		return true;
		
	}

	public function wp_user_login_collection_params() {
		$params = array();
		$params['iv'] = array(
			'required' => true,
			'default'	=> '',
			'description'	=> "授权登录，用户基本信息.",
			'type'	=>	 "string"
		);
		$params['code'] = array(
			'required' => true,
			'default'	=> '',
			'description'	=> "登录凭证（有效期五分钟）",
			'type'	=>	 "string"
		);
		$params['encryptedData'] = array(
			'required' => true,
			'default'	=> '',
			'description'	=> "授权登录，用户基本信息",
			'type'	=>	 "string"
		);
		return $params;
	}

	public function wp_qq_user_auth_login( $request ) {

		date_default_timezone_set( datetime_timezone() );

		$code = $request->get_param('code');
		$iv = $request->get_param('iv');
		$encryptedData = $request->get_param('encryptedData');
		
		if( empty($code) ) {
			return new WP_Error( 'error', '用户登录 code 参数错误', array( 'status' => 403 ) );
		}

		if( empty($iv) ) {
			return new WP_Error( 'error', '缺少加密算法的初始向量', array( 'status' => 403 ) );
		}

		if( empty($encryptedData) ) {
			return new WP_Error( 'error', '缺少用户信息的加密数据', array( 'status' => 403 ) );
		}
		
		$appid 			= wp_miniprogram_option('qq_appid');
		$appsecret 		= wp_miniprogram_option('qq_secret');
		$role 			= wp_miniprogram_option('use_role');

		$args = array(
			'appid' => $appid,
			'secret' => $appsecret,
			'js_code' => $code,
			'grant_type' => 'authorization_code'
		);
		
		$api = 'https://api.q.qq.com/sns/jscode2session';
		$url = add_query_arg( $args, $api );
		$remote = wp_remote_get( $url );
		if( is_wp_error($remote) || !isset($remote['body']) ) {
			return new WP_Error( 'error', '获取授权 OpenID 和 Session 错误', array( 'status' => 403, 'message' => $remote ) );
		}

		$body = stripslashes( $remote['body'] );
		$session = json_decode( $body, true );
		if( $session['errcode'] != 0 ) {
			return new WP_Error( 'error', '获取用户信息错误,请检查设置', array( 'status' => 403, 'message' => $session ) );
		}
		
		$session_key = $session['session_key'];
		$openId = $session['openid'];
		$unionId = $session['unionid'];

		$user_id = 0;
		$token = MP_Auth::generate_session( );
		$expire = isset($token['expire_in']) ? $token['expire_in'] : date( 'Y-m-d H:i:s', time() + 86400 );
		$token_id = isset($token['session_key']) ? $token['session_key'] : $session_key;
		$user_pass = wp_generate_password( 16, false );
		
		if( $unionId ) {
			$users = get_userdata_by_meta( 'unionid', $unionId );
			if( isset($users->user_id) ) {
				$user_id = $users->user_id;
				update_user_meta( $user_id, 'openid', $openId );
				update_user_meta( $user_id, 'unionid', $unionId );
				update_user_meta( $user_id, 'expire_in', $expire );
				update_user_meta( $user_id, 'session_key', $token_id );
				update_user_meta( $user_id, 'platform', 'tencent');
			} else if( username_exists($openId) ) {
				$user = get_user_by( 'login', $openId );
				$user_id = $user->ID;			
				update_user_meta( $user_id, 'openid', $openId );
				update_user_meta( $user_id, 'unionid', $unionId );
				update_user_meta( $user_id, 'expire_in', $expire );
				update_user_meta( $user_id, 'session_key', $token_id );
				add_user_meta( $user_id, 'platform', 'tencent');
			} else {
				$users = get_userdata_by_meta( 'openid', $openId );
				if( isset( $users->user_id ) ) {
					$user_id = $users->user_id;
					update_user_meta( $user_id, 'openid', $openId );
					update_user_meta( $user_id, 'unionid', $unionId );
					update_user_meta( $user_id, 'expire_in', $expire );
					update_user_meta( $user_id, 'session_key', $token_id );
					update_user_meta( $user_id, 'platform', 'tencent');
				} else {
					$auth = MP_Auth::decryptData( $appid, $session_key, urldecode($encryptedData), urldecode($iv), $data );
					if( $auth != 0 ) {
						return new WP_Error( 'error', '用户信息解密错误', array( 'status' => 403, 'errmsg' => $auth ) );
					}
					$user_data = json_decode( $data, true );
					$userdata = array(
						'user_login' 			=> $openId,
						'nickname' 				=> $user_data['nickName'],
						'first_name'			=> $user_data['nickName'],
						'user_nicename' 		=> $openId,
						'display_name' 			=> $user_data['nickName'],
						'user_email' 			=> date('Ymdhms').'@qq.com',
						'role' 					=> $role,
						'user_pass' 			=> $user_pass,
						'gender'				=> $user_data['gender'],
						'openid'				=> $openId,
						'city'					=> $user_data['city'],
						'avatar' 				=> $user_data['avatarUrl'],
						'province'				=> $user_data['province'],
						'country'				=> $user_data['country'],
						'language'				=> $user_data['language'],
						'expire_in'				=> $expire
					);
					$user_id = wp_insert_user( $userdata );			
					if( is_wp_error( $user_id ) ) {
						return new WP_Error( 'error', '创建用户失败', array( 'status' => 400, 'error' => $user_id ) );				
					}
					add_user_meta( $user_id, 'unionid', $unionId );
					add_user_meta( $user_id, 'session_key', $token_id );
					add_user_meta( $user_id, 'platform', 'tencent');
				}
			}
		} else if( username_exists($openId) ) {
			$user = get_user_by( 'login', $openId );
			$user_id = $user->ID;
			update_user_meta( $user_id, 'openid', $openId );
			update_user_meta( $user_id, 'unionid', $unionId );
			update_user_meta( $user_id, 'expire_in', $expire );
			update_user_meta( $user_id, 'session_key', $token_id );
			update_user_meta( $user_id, 'platform', 'tencent');
		} else {
			$users = get_userdata_by_meta( 'openid', $openId );
			if( isset( $users->user_id ) ) {
				$user_id = $users->user_id;
				update_user_meta( $user_id, 'openid', $openId );
				update_user_meta( $user_id, 'unionid', $unionId );
				update_user_meta( $user_id, 'expire_in', $expire );
				update_user_meta( $user_id, 'session_key', $token_id );
				update_user_meta( $user_id, 'platform', 'tencent');
				do_action( 'mp_qq_auth_login', $user_id );
			} else {
				$auth = MP_Auth::decryptData( $appid, $session_key, urldecode($encryptedData), urldecode($iv), $data );
				if( $auth != 0 ) {
					return new WP_Error( 'error', '用户信息解密错误', array( 'status' => 403, 'errmsg' => $auth ) );
				}
				$user_data = json_decode( $data, true );
				$userdata = array(
					'user_login' 			=> $openId,
					'nickname' 				=> $user_data['nickName'],
					'first_name'			=> $user_data['nickName'],
					'user_nicename' 		=> $openId,
					'display_name' 			=> $user_data['nickName'],
					'user_email' 			=> date('Ymdhms').'@qq.com',
					'role' 					=> $role,
					'user_pass' 			=> $user_pass,
					'gender'				=> $user_data['gender'],
					'openid'				=> $openId,
					'city'					=> $user_data['city'],
					'avatar' 				=> $user_data['avatarUrl'],
					'province'				=> $user_data['province'],
					'country'				=> $user_data['country'],
					'language'				=> $user_data['language'],
					'expire_in'				=> $expire
				);
				$user_id = wp_insert_user( $userdata );			
				if( is_wp_error( $user_id ) ) {
					return new WP_Error( 'error', '创建用户失败', array( 'status' => 400, 'error' => $user_id ) );				
				}
				add_user_meta( $user_id, 'unionid', $unionId );
				add_user_meta( $user_id, 'session_key', $token_id );
				add_user_meta( $user_id, 'platform', 'tencent');
			}
		}
		
		$current_user = get_user_by( 'ID', $user_id );
		if( is_multisite() ) {
			$blog_id = get_current_blog_id();
			$roles = ( array )$current_user->roles[$blog_id];
		} else {
			$roles = ( array )$current_user->roles;
		}

		wp_set_current_user( $user_id, $current_user->user_login );
		wp_set_auth_cookie( $user_id, true );
		
		$user = array(
			"user"	=> array(
				"userId"		=> $user_id,
				"nickName"		=> $current_user->nickname,
				"openId"		=> $openId,
				"avatarUrl" 	=> $current_user->avatar,
				"gender"		=> $current_user->gender,
				"city"			=> $current_user->city,
				"province"		=> $current_user->province,
				"country"		=> $current_user->country,
				"language"		=> $current_user->language,
				"role"			=> $roles[0],
				'platform'		=> $current_user->platform,
				"description"	=> $current_user->description
			),
			"access_token" => base64_encode( $token_id ),
			"expired_in" => strtotime( $expire ) * 1000
			
		);
		$response = rest_ensure_response( $user );
		return $response;

	}

	public function wp_baidu_user_auth_login( $request ) {

		date_default_timezone_set( datetime_timezone() );
		
		$iv = $request->get_param('iv');
		$code = $request->get_param('code');
		$encryptedData = $request->get_param('encryptedData');
		
		if( empty($code) ) {
			return new WP_Error( 'error', '用户登录 code 参数错误', array( 'status' => 403 ) );
		}

		if( empty($iv) ) {
			return new WP_Error( 'error', '缺少加密算法的初始向量', array( 'status' => 403 ) );
		}

		if( empty($encryptedData) ) {
			return new WP_Error( 'error', '缺少用户信息的加密数据', array( 'status' => 403 ) );
		}

		$appkey 		= wp_miniprogram_option('bd_appkey');
		$appsecret 		= wp_miniprogram_option('bd_secret');
		$role 			= wp_miniprogram_option('use_role');

		$args = array(
			'client_id' => $appkey,
			'sk' => $appsecret,
			'code' => $code
		);

		$api = 'https://spapi.baidu.com/oauth/jscode2sessionkey';
		$url = add_query_arg( $args, $api );
		$remote = wp_remote_request( $url, array( 'method' => 'POST' ) );
		if( is_wp_error($remote) ) {
			return new WP_Error( 'error', '获取授权 OpenID 和 Session 错误', array( 'status' => 403, 'message' => $remote ) );
		}

		$body = wp_remote_retrieve_body( $remote );
		$session = json_decode( $body, true );
		$session_key = $session['session_key'];
		$openId = $session['openid'];
		
		$user_id = 0;
		$token = MP_Auth::generate_session( );
		$expire = isset($token['expire_in']) ? $token['expire_in'] : date( 'Y-m-d H:i:s', time() + 86400 );
		$token_id = isset($token['session_key']) ? $token['session_key'] : $session_key;
		$user_pass = wp_generate_password( 16, false );
		
		if( username_exists($openId) ) {
			$user = get_user_by( 'login', $openId );
			$user_id = $user->ID;			
			update_user_meta( $user_id, 'openid', $openId );
			update_user_meta( $user_id, 'expire_in', $expire );
			update_user_meta( $user_id, 'session_key', $token_id );
			add_user_meta( $user_id, 'platform', 'baidu');
		} else {
			$users = get_userdata_by_meta( 'openid', $openId );
			if( isset( $users->user_id ) ) {
				$user_id = $users->user_id;
				update_user_meta( $user_id, 'openid', $openId );
				update_user_meta( $user_id, 'expire_in', $expire );
				update_user_meta( $user_id, 'session_key', $token_id );
				update_user_meta( $user_id, 'platform', 'baidu');
			} else {
				$auth = MP_Auth::decrypt(urldecode($encryptedData), urldecode($iv), $appkey, $session_key);
				if( ! $auth ) {
					return new WP_Error( 'error', '用户信息解密错误', array( 'status' => 403, 'errmsg' => $auth ) );
				}
				$user_data = json_decode( $auth, true );
				$userdata = array(
					'user_login' 			=> $openId,
					'nickname' 				=> $user_data['nickname'],
					'first_name'			=> $user_data['nickname'],
					'user_nicename' 		=> $openId,
					'display_name' 			=> $user_data['nickname'],
					'user_email' 			=> date('Ymdhms').'@baidu.com',
					'role' 					=> $role,
					'user_pass' 			=> $user_pass,
					'gender'				=> $user_data['sex'],
					'openid'				=> $openId,
					'avatar' 				=> $user_data['headimgurl'],
					'expire_in'				=> $expire
				);
				$user_id = wp_insert_user( $userdata );			
				if( is_wp_error( $user_id ) ) {
					return new WP_Error( 'error', '创建用户失败', array( 'status' => 400, 'error' => $user_id ) );				
				}
				add_user_meta( $user_id, 'session_key', $token_id );
				add_user_meta( $user_id, 'platform', 'baidu');
			}
		}

		$current_user = get_user_by( 'ID', $user_id );
		if( is_multisite() ) {
			$blog_id = get_current_blog_id();
			$roles = ( array )$current_user->roles[$blog_id];
		} else {
			$roles = ( array )$current_user->roles;
		}

		wp_set_current_user( $user_id, $current_user->user_login );
		wp_set_auth_cookie( $user_id, true );
		
		$user = array(
			"user"	=> array(
				"userId"		=> $user_id,
				"nickName"		=> $current_user->nickname,
				"openId"		=> $openId,
				"avatarUrl" 	=> $current_user->avatar,
				"gender"		=> $current_user->gender,
				"role"			=> $roles[0],
				'platform'		=> $current_user->platform,
				"description"	=> $current_user->description
			),
			"access_token" => base64_encode( $token_id ),
			"expired_in" => strtotime( $expire ) * 1000
		);

		$response = rest_ensure_response( $user );
		return $response;

	}

	public function wp_toutiao_user_auth_login( $request ) {

		date_default_timezone_set( datetime_timezone() );
		
		$iv = $request->get_param('iv');
		$code = $request->get_param('code');
		$encryptedData = $request->get_param('encryptedData');
		
		if( empty($code) ) {
			return new WP_Error( 'error', '用户登录 code 参数错误', array( 'status' => 403 ) );
		}

		if( empty($iv) ) {
			return new WP_Error( 'error', '缺少加密算法的初始向量', array( 'status' => 403 ) );
		}

		if( empty($encryptedData) ) {
			return new WP_Error( 'error', '缺少用户信息的加密数据', array( 'status' => 403 ) );
		}

		$appid 			= wp_miniprogram_option('tt_appid');
		$secret 		= wp_miniprogram_option('tt_secret');
		$role 			= wp_miniprogram_option('use_role');
		
		$args = array(
			'appid' => $appid,
			'secret' => $secret,
			'code' => $code
		);

		$api = 'https://developer.toutiao.com/api/apps/jscode2session';
		$url = add_query_arg( $args, $api );
		$remote = wp_remote_get( $url );
		if( is_wp_error($remote) ) {
			return new WP_Error( 'error', '获取授权 OpenID 和 Session 错误', array( 'status' => 403, 'message' => $remote ) );
		}

		$body = wp_remote_retrieve_body( $remote );
		$session = json_decode( $body, true );
		$session_key = $session['session_key'];
		$openId = $session['openid'];
		
		$user_id = 0;
		$token = MP_Auth::generate_session( );
		$expire = isset($token['expire_in']) ? $token['expire_in'] : date( 'Y-m-d H:i:s', time() + 86400 );
		$token_id = isset($token['session_key']) ? $token['session_key'] : $session_key;
		$user_pass = wp_generate_password( 16, false );
		
		if( username_exists($openId) ) {
			$user = get_user_by( 'login', $openId );
			$user_id = $user->ID;			
			update_user_meta( $user_id, 'openid', $openId );
			update_user_meta( $user_id, 'expire_in', $expire );
			update_user_meta( $user_id, 'session_key', $token_id );
			add_user_meta( $user_id, 'platform', 'toutiao');
		} else {
			$users = get_userdata_by_meta( 'openid', $openId );
			if( isset( $users->user_id ) ) {
				$user_id = $users->user_id;
				update_user_meta( $user_id, 'openid', $openId );
				update_user_meta( $user_id, 'expire_in', $expire );
				update_user_meta( $user_id, 'session_key', $token_id );
				update_user_meta( $user_id, 'platform', 'toutiao');
			} else {
				$auth = MP_Auth::decryptData($appid, $session_key, urldecode($encryptedData), urldecode($iv), $data);
				if( $auth != 0 ) {
					return new WP_Error( 'error', '用户信息解密错误', array( 'status' => 403, 'errmsg' => $auth ) );
				}
				$user_data = json_decode( $data, true );
				$userdata = array(
					'user_login' 			=> $openId,
					'nickname' 				=> $user_data['nickName'],
					'first_name'			=> $user_data['nickName'],
					'user_nicename' 		=> $openId,
					'display_name' 			=> $user_data['nickName'],
					'user_email' 			=> date('Ymdhms').'@toutiao.com',
					'role' 					=> $role,
					'user_pass' 			=> $user_pass,
					'gender'				=> $user_data['gender'],
					'openid'				=> $openId,
					'city'					=> $user_data['city'],
					'avatar' 				=> $user_data['avatarUrl'],
					'province'				=> $user_data['province'],
					'country'				=> $user_data['country'],
					'language'				=> $user_data['language'],
					'expire_in'				=> $expire
				);
				$user_id = wp_insert_user( $userdata );
				if( is_wp_error( $user_id ) ) {
					return new WP_Error( 'error', '创建用户失败', array( 'status' => 400, 'error' => $user_id ) );				
				}
				add_user_meta( $user_id, 'session_key', $token_id );
				add_user_meta( $user_id, 'platform', 'toutiao');
			}
		}

		$current_user = get_user_by( 'ID', $user_id );
		if( is_multisite() ) {
			$blog_id = get_current_blog_id();
			$roles = ( array )$current_user->roles[$blog_id];
		} else {
			$roles = ( array )$current_user->roles;
		}

		wp_set_current_user( $user_id, $current_user->user_login );
		wp_set_auth_cookie( $user_id, true );
		
		$user = array(
			"user"	=> array(
				"userId"		=> $user_id,
				"nickName"		=> $current_user->nickname,
				"openId"		=> $openId,
				"avatarUrl" 	=> $current_user->avatar,
				"gender"		=> $current_user->gender,
				"city"			=> $current_user->city,
				"province"		=> $current_user->province,
				"country"		=> $current_user->country,
				"language"		=> $current_user->language,
				"role"			=> $roles[0],
				'platform'		=> $current_user->platform,
				"description"	=> $current_user->description
			),
			"access_token" => base64_encode( $token_id ),
			"expired_in" => strtotime( $expire ) * 1000
		);
		
		$response = rest_ensure_response( $user );
		return $response;

	}

}